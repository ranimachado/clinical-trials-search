<?php

/**
 * @file
 * Contains ct_facility_investigator_entity.page.inc..
 *
 * Page callback for CT Facility Investigator entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for CT Facility Investigator templates.
 *
 * Default template: ct_facility_investigator_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_ct_facility_investigator_entity(array &$variables) {
  // Fetch CtFacilityInvestigatorEntity Entity Object.
  $ct_facility_investigator_entity = $variables['elements']['#ct_facility_investigator_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

<?php

/**
 * @file
 * Contains ct_categories_entity.page.inc..
 *
 * Page callback for CT Categories entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for CT Categories templates.
 *
 * Default template: ct_categories_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_ct_categories_entity(array &$variables) {
  // Fetch CtCategoriesEntity Entity Object.
  $ct_categories_entity = $variables['elements']['#ct_categories_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

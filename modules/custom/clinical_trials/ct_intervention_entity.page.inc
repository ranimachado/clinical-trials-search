<?php

/**
 * @file
 * Contains ct_intervention_entity.page.inc..
 *
 * Page callback for CT Intervention entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for CT Intervention templates.
 *
 * Default template: ct_intervention_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_ct_intervention_entity(array &$variables) {
  // Fetch CtInterventionEntity Entity Object.
  $ct_intervention_entity = $variables['elements']['#ct_intervention_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

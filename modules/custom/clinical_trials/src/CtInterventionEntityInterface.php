<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\CtInterventionEntityInterface.
 */

namespace Drupal\clinical_trials;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining CT Intervention entities.
 *
 * @ingroup clinical_trials
 */
interface CtInterventionEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the CT Intervention name.
   *
   * @return string
   *   Name of the CT Intervention.
   */
  public function getName();

  /**
   * Sets the CT Intervention name.
   *
   * @param string $name
   *   The CT Intervention name.
   *
   * @return \Drupal\clinical_trials\CtInterventionEntityInterface
   *   The called CT Intervention entity.
   */
  public function setName($name);

  /**
   * Gets the CT Intervention creation timestamp.
   *
   * @return int
   *   Creation timestamp of the CT Intervention.
   */
  public function getCreatedTime();

  /**
   * Sets the CT Intervention creation timestamp.
   *
   * @param int $timestamp
   *   The CT Intervention creation timestamp.
   *
   * @return \Drupal\clinical_trials\CtInterventionEntityInterface
   *   The called CT Intervention entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the CT Intervention published status indicator.
   *
   * Unpublished CT Intervention are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the CT Intervention is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a CT Intervention.
   *
   * @param bool $published
   *   TRUE to set this CT Intervention to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\clinical_trials\CtInterventionEntityInterface
   *   The called CT Intervention entity.
   */
  public function setPublished($published);

}

<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\CtLocationEntityAccessControlHandler.
 */

namespace Drupal\clinical_trials;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the CT Location entity.
 *
 * @see \Drupal\clinical_trials\Entity\CtLocationEntity.
 */
class CtLocationEntityAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\clinical_trials\CtLocationEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished ct location entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published ct location entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit ct location entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete ct location entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add ct location entities');
  }

}

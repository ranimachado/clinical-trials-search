<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Plugin\Block\ClinicalTrialsSearchBlock.
 */

namespace Drupal\clinical_trials\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * Provides a 'Clinical Trials Search' block.
 *
 * @Block(
 *  id = "clinical_trials_search_block",
 *  admin_label = @Translation("Clinical Trials Search"),
 * )
 */
class ClinicalTrialsSearchBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\clinical_trials\Form\ClinicalTrialsSearchForm');
    return $form;
  }

}

<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\ClinicalTrialsEntityInterface.
 */

namespace Drupal\clinical_trials;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Clinical Trials entities.
 *
 * @ingroup clinical_trials
 */
interface ClinicalTrialsEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Clinical Trials name.
   *
   * @return string
   *   Name of the Clinical Trials.
   */
  public function getName();

  /**
   * Sets the Clinical Trials name.
   *
   * @param string $name
   *   The Clinical Trials name.
   *
   * @return \Drupal\clinical_trials\ClinicalTrialsEntityInterface
   *   The called Clinical Trials entity.
   */
  public function setName($name);

  /**
   * Gets the Clinical Trials creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Clinical Trials.
   */
  public function getCreatedTime();

  /**
   * Sets the Clinical Trials creation timestamp.
   *
   * @param int $timestamp
   *   The Clinical Trials creation timestamp.
   *
   * @return \Drupal\clinical_trials\ClinicalTrialsEntityInterface
   *   The called Clinical Trials entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Clinical Trials published status indicator.
   *
   * Unpublished Clinical Trials are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Clinical Trials is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Clinical Trials.
   *
   * @param bool $published
   *   TRUE to set this Clinical Trials to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\clinical_trials\ClinicalTrialsEntityInterface
   *   The called Clinical Trials entity.
   */
  public function setPublished($published);

}

<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\CtStudyContactEntityAccessControlHandler.
 */

namespace Drupal\clinical_trials;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the CT Study Contact entity.
 *
 * @see \Drupal\clinical_trials\Entity\CtStudyContactEntity.
 */
class CtStudyContactEntityAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\clinical_trials\CtStudyContactEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished ct study contact entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published ct study contact entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit ct study contact entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete ct study contact entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add ct study contact entities');
  }

}

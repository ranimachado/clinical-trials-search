<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\ClinicalTrialImportService.
 */

namespace Drupal\clinical_trials;

use Drupal;
use Drupal\Component\Utility\Xss;
use Drupal\taxonomy\Entity\Term;
use Drupal\clinical_trials\Entity\ClinicalTrialsEntity;
use Drupal\clinical_trials\Entity\CtCollaboratorSponsorEntity;
use Drupal\clinical_trials\Entity\CtFacilityInvestigatorEntity;
use Drupal\clinical_trials\Entity\CtInterventionEntity;
use Drupal\clinical_trials\Entity\CtLocationEntity;
use Drupal\clinical_trials\Entity\CtStudyContactEntity;

/**
 * Class ClinicalTrialImportService.
 *
 * @package Drupal\clinical_trials
 */
class ClinicalTrialImportService {

  private $apiUrl = NULL;
  private $notRelevantFields = array(
    'id',
    'uuid',
    'langcode',
    'user_id',
    'ctid',
    'status',
    'created',
    'changed',
  );

  /**
   * Constructor.
   */
  public function __construct() {
    $this->apiUrl = \Drupal::config('clinical_trials.settings')->get('clinicaltrial_gov_api_url');
  }

  /**
   * Imports a specific CT from clinicaltrial.gov.
   *
   * @param string $gov_ctid
   *   The Clinical Trial ID.
   *
   * @return boolean $result
   *   A flag indicating whether or not the process succeeded.
   */
  public function importClinicalTrial($gov_ctid = NULL) {
    $result = TRUE;
    if ($this->ctExists($gov_ctid)) {
      drupal_set_message(t('This Clinical Trial cannot be imported because it already has a record in the database: %id', array('%id' => $gov_ctid)), 'error');
    }
    else {
      if (!$ct_data = $this->loadDataAndSaveClinicalTrial($gov_ctid)) {
        drupal_set_message(t('This Clinical Trial was not found in clinicaltrial.gov: %id', array('%id' => $gov_ctid)), 'error');
      }
      else {
        drupal_set_message(t('Clinical Trial imported to the system: %id', array('%id' => $gov_ctid)), 'status');
      }
    }
    return $result;
  }

  /**
   * Check if CT was already imported.
   *
   * @param string $gov_ctid
   *   The Clinical Trial ID.
   *
   * @return boolean $result
   *   A flag indicating whether or not the CT exists.
   */
  private function ctExists($gov_ctid) {
    $query = \Drupal::database()->select('clinical_trials_entity', 'ct');
    $query->addField('ct', 'gov_id');
    $query->condition('ct.gov_id', $gov_ctid);
    $result = $query->execute()->fetchField();
    return $result;
  }

  /**
   * Calls clinicaltrial.gov to get the CT.
   *
   * @param string $gov_ctid
   *   The Clinical Trial ID.
   *
   * @return array $result
   *   The data pulled from clinicaltrial.gov.
   */
  public function getCtFromClinicalTrialsDotGov($gov_ctid) {
    $url = sprintf($this->apiUrl, $gov_ctid);
    return $xml_obj = simplexml_load_file($url);
  }

  /**
   * Loads data from external source and save on database.
   *
   * @param string $gov_ctid
   *   The Clinical Trial ID.
   *
   * @return bool
   *   Whether or not the entity was successfully saved.
   */
  private function loadDataAndSaveClinicalTrial($gov_ctid) {
    $result = FALSE;
    if ($xml_obj = $this->getCtFromClinicalTrialsDotGov($gov_ctid)) {
      $result = $this->saveClinicalTrial($xml_obj);
    }
    return $result;
  }

  /**
   * Populates Cloinical Trial parent entity and save it.
   *
   * @param object $xml_obj
   *   The XML object.
   *
   * @return bool
   *   Whether or not the entity was successfully saved.
   */
  private function saveClinicalTrial($xml_obj) {
    $values = $this->extractClinicalTrialsValuesFromXml($xml_obj);
    $this->prepareValuesForSaving('clinical_trials_entity', $values);
    $entity = ClinicalTrialsEntity::create($values);
    if ($result = $entity->save()) {
      $this->saveAggregatedEntities($xml_obj, $entity->id());
    }
    return $result;
  }

  /**
   * Populates all aggregated entities and save them.
   *
   * @param object $xml_obj
   *   The XML object.
   * @param int $ctid
   *   The parent Clinical Trial ID.
   */
  private function saveAggregatedEntities($xml_obj, $ctid) {
    // Ct Collaborator Sponsor entity.
    $multi_values = $this->extractCtCollaboratorSponsorValuesFromXml($xml_obj, $ctid);
    foreach ($multi_values as $values) {
      $this->prepareValuesForSaving('ct_collaborator_sponsor_entity', $values);
      $entity = CtCollaboratorSponsorEntity::create($values);
      $entity->save();
    }
    // Ct Facility Investigator entity.
    $multi_values = $this->extractCtFacilityInvestigatorValuesFromXml($xml_obj, $ctid);
    foreach ($multi_values as $values) {
      $this->prepareValuesForSaving('ct_facility_investigator_entity', $values);
      $entity = CtFacilityInvestigatorEntity::create($values);
      $entity->save();
    }
    // Ct Intervention entity.
    $multi_values = $this->extractCtInterventionValuesFromXml($xml_obj, $ctid);
    foreach ($multi_values as $values) {
      $this->prepareValuesForSaving('ct_intervention_entity', $values);
      $entity = CtInterventionEntity::create($values);
      $entity->save();
    }
    // Ct Location entity.
    $multi_values = $this->extractCtLocationValuesFromXml($xml_obj, $ctid);
    foreach ($multi_values as $values) {
      $this->prepareValuesForSaving('ct_location_entity', $values);
      $entity = CtLocationEntity::create($values);
      $entity->save();
    }
    // Ct Study Contact entity.
    $multi_values = $this->extractCtStudyContactValuesFromXml($xml_obj, $ctid);
    foreach ($multi_values as $values) {
      $this->prepareValuesForSaving('ct_study_contact_entity', $values);
      $entity = CtStudyContactEntity::create($values);
      $entity->save();
    }
  }

  /**
   * Extracts the values for Clinical Trials entity from the XML object.
   *
   * @param object $xml_obj
   *   The XML object.
   *
   * @return array $result;
   *   An array with the extracted values.
   */
  public function extractClinicalTrialsValuesFromXml($xml_obj) {
    $criteria = $this->decompoundCriteriaFields($xml_obj->eligibility->criteria);
    $common_summary = $this->concatenateTextBlocks($xml_obj->brief_summary);
    $common_description = $this->concatenateTextBlocks($xml_obj->detailed_description);
    $result = array(
      'uuid' => $this->generateUuid(),
      'langcode' => 'en',
      'user_id' => Drupal::currentUser()->id(),
      'name' => $xml_obj->brief_title->__toString(),
      'body' => array(
        'value' => $common_description,
        'summary' => $common_summary,
        'format' => 'full_html',
      ),
      'gov_id' => $xml_obj->id_info->nct_id->__toString(),
      'last_update_date' => date('Y-m-d', strtotime($xml_obj->lastchanged_date->__toString())) . 'T00:00:00',
      'overall_status' => $this->getOverallStatus($xml_obj->overall_status->__toString()),
      'lead_sponsor_agency_name' => $xml_obj->sponsors->lead_sponsor->agency->__toString(),
      'lead_sponsor_agency_class' => $xml_obj->sponsors->lead_sponsor->agency_class->__toString(),
      'official_title' => $xml_obj->official_title->__toString(),
      'pq_min_age' => $xml_obj->eligibility->minimum_age->__toString(),
      'pq_max_age' => $xml_obj->eligibility->maximum_age->__toString(),
      'pq_gender' => $xml_obj->eligibility->gender->__toString(),
      'pq_healthy_volunteers' => $xml_obj->eligibility->healthy_volunteers->__toString(),
      'start_date' => $xml_obj->start_date->__toString(),
      'end_date' => $xml_obj->completion_date->__toString(),
      'inclusion_criteria' => array(
        'value' => $criteria['inclusion'],
        'format' => 'basic_html',
      ),
      'exclusion_criteria' => array(
        'value' => $criteria['exclusion'],
        'format' => 'basic_html',
      ),
      'phase' => $xml_obj->phase->__toString(),
      'study_type' => $xml_obj->study_type->__toString(),
      'study_design' => $this->compoundStudyDesign($xml_obj->study_design_info),
      'enrollment' => $xml_obj->enrollment->__toString(),
      'description_adear_brief' => array(
        'value' => $common_summary,
        'format' => 'full_html',
      ),
      'description_adear_detailed' => array(
        'value' => $common_description,
        'format' => 'full_html',
      ),
      'trial_link' => $this->compoundTrialLinks($xml_obj),
      'trial_ref' => $this->compoundTrialReferences($xml_obj),
      'keywords' => $this->compoundKeywords($xml_obj),
      'criteria' => $this->concatenateTextBlocks($xml_obj->eligibility->criteria),
      'brief_description' => $common_summary,
    );

    return $result;
  }

  /**
   * Extracts the values for Collaborator Sponsor entity from the XML object.
   *
   * @param object $xml_obj
   *   The XML object.
   *
   * @return array $result
   *   An array with the extracted values.
   */
  private function extractCtCollaboratorSponsorValuesFromXml($xml_obj, $ctid) {
    $result = array();
    foreach ($xml_obj->sponsors->collaborator as $item) {
      $result[] = array(
        'uuid' => $this->generateUuid(),
        'langcode' => 'en',
        'user_id' => Drupal::currentUser()->id(),
        'ctid' => $ctid,
        'name' => $item->agency->__toString(),
        'class' => $item->agency_class->__toString(),
      );
    }
    return $result;
  }

  /**
   * Extracts the values for Facility Investigator entity from the XML object.
   *
   * @param object $xml_obj
   *   The XML object.
   *
   * @return array $result
   *   An array with the extracted values.
   */
  private function extractCtFacilityInvestigatorValuesFromXml($xml_obj, $ctid) {
    $result = array();
    foreach ($xml_obj->overall_official as $item) {
      $result[] = array(
        'uuid' => $this->generateUuid(),
        'langcode' => 'en',
        'user_id' => Drupal::currentUser()->id(),
        'ctid' => $ctid,
        'name' => $item->last_name->__toString(),
        'role' => $item->role->__toString(),
        'affiliation' => $item->affiliation->__toString(),
      );
    }
    return $result;
  }

  /**
   * Extracts the values for Intervention entity from the XML object.
   *
   * @param object $xml_obj
   *   The XML object.
   *
   * @return array $result
   *   An array with the extracted values.
   */
  private function extractCtInterventionValuesFromXml($xml_obj, $ctid) {
    $result = array();
    foreach ($xml_obj->intervention as $item) {
      $result[] = array(
        'uuid' => $this->generateUuid(),
        'langcode' => 'en',
        'user_id' => Drupal::currentUser()->id(),
        'ctid' => $ctid,
        'name' => $item->intervention_name->__toString(),
        'description' => $item->description->__toString(),
        'arm_group_label' => $item->arm_group_label->__toString(),
      );
    }
    return $result;
  }

  /**
   * Extracts the values for Location entity from the XML object.
   *
   * @param object $xml_obj
   *   The XML object.
   *
   * @return array $result
   *   An array with the extracted values.
   */
  public function extractCtLocationValuesFromXml($xml_obj, $ctid) {
    $result = array();
    foreach ($xml_obj->location as $item) {
      if (@$item->contact) {
        $contact = array(
          'primary_contact_name' => $item->contact->last_name->__toString(),
          'primary_contact_phone' => $item->contact->phone->__toString(),
          'primary_contact_email' => $item->contact->email->__toString(),
        );
      }
      else {
        $contact = array();
      }
      $result[] = array(
        'uuid' => $this->generateUuid(),
        'langcode' => 'en',
        'user_id' => Drupal::currentUser()->id(),
        'ctid' => $ctid,
        'name' => $item->facility->name->__toString(),
        'city' => $item->facility->address->city->__toString(),
        'state' => $item->facility->address->state->__toString(),
        'zip' => $item->facility->address->zip->__toString(),
        'country' => $item->facility->address->country->__toString(),
        'location_status' => $item->status->__toString(),
      ) + $contact;
    }
    return $result;
  }

  /**
   * Extracts the values for Study Contact entity from the XML object.
   *
   * @param object $xml_obj
   *   The XML object.
   *
   * @return array $result
   *   An array with the extracted values.
   */
  private function extractCtStudyContactValuesFromXml($xml_obj, $ctid) {
    $result = array();
    foreach (['overall_contact', 'overall_contact_backup'] as $node_name) {
      foreach ($xml_obj->$node_name as $item) {
        $result[] = array(
          'uuid' => $this->generateUuid(),
          'langcode' => 'en',
          'user_id' => Drupal::currentUser()->id(),
          'ctid' => $ctid,
          'name' => $item->last_name->__toString(),
          'phone' => $item->phone->__toString(),
          'email' => $item->email->__toString(),
        );
      }
    }
    return $result;
  }

  /**
   * Prepare values to be saved.
   *
   * String fields will have its values length checked and
   * truncated if necessary.
   *
   * @param string $entity_type
   *   The type of the Entity.
   * @param array $values
   *   The values to be prepared to be saved.
   */
  public function prepareValuesForSaving($entity_type, &$values) {
    $fields_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type, NULL);
    foreach ($values as $field_name => &$value) {
      if (!in_array($field_name, $this->notRelevantFields)) {
        if (is_array($value)) {
          $value = Xss::filter($value);
        }
        else {
          $type = $fields_definitions[$field_name]->getType();
          $settings = $fields_definitions[$field_name]->getSettings();
          switch ($type) {
            case 'string':
              $value = substr($value, 0, $settings['max_length']);
              $value = Xss::filter($value);
              break;

            case 'string_long':
              $value = Xss::filter($value);
              break;
          }
        }
      }
    }
    $d = time();
    $values = $values + array(
      'status' => 1,
      'created' => $d,
      'changed' => $d,
    );
  }

  /**
   * Generates a universal unique id.
   *
   * @return string
   *   The uuid.
   */
  protected function generateUuid() {
    $result = sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
      mt_rand(0, 0xffff), mt_rand(0, 0xffff),
      mt_rand(0, 0xffff),
      mt_rand(0, 0x0fff) | 0x4000,
      mt_rand(0, 0x3fff) | 0x8000,
      mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
    return $result;
  }

  /**
   * Concatenates content split in several <textblock>s.
   *
   * @param object $text_blocks
   *   The parent XML node containing the <textblock>s.
   *
   * @return string
   *   The concatenated text.
   */
  private function concatenateTextBlocks($text_blocks) {
    $result = array();
    foreach ($text_blocks->textblock as $text_block) {
      $s = trim(Xss::filter($text_block->__toString()));
      $s = strtr($s, array(
        "\r" => " ",
        "\n" => " ",
        "\t" => " ",
        str_repeat(' ', 6) => '',
      ));
      $result[] = $s;
    }
    return implode('<br/>', $result);
  }

  /**
   * Gets the id of an existing term or create new term and get its id.
   *
   * @param string $name
   *   The term name.
   *
   * @return int
   *   The term id.
   */
  public function getOverallStatus($name) {
    $result = 0;
    if ($terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['name' => $name])) {
      $term = reset($terms);
      $result = $term->id();
    }
    else {
      $term = Term::create([
        'vid' => 'overall_status',
        'name' => $name,
      ]);
      if ($term->save()) {
        $result = $term->id();
      }
    }
    return $result;
  }

  /**
   * Decompounds Criteria node in two separated texts: Inclusion and Exclusion.
   *
   * @param object $criteria
   *   The Criteria text.
   *
   * @return array
   *   The text decompound in two elements.
   *
   * @TODO: Check if it's working propelly.
   */
  private function decompoundCriteriaFields($criteria) {
    $result = array(
      'inclusion' => NULL,
      'exclusion' => NULL,
    );
    $s = $this->concatenateTextBlocks($criteria);
    $p1 = '/key inclusion criteria|inclusion criteria/i';
    preg_match_all($p1, $s, $matches1, PREG_OFFSET_CAPTURE);
    $p2 = '/key exclusion criteria|exclusion criteria/i';
    preg_match_all($p2, $s, $matches2, PREG_OFFSET_CAPTURE);
    if ($matches1 && $matches2 && count($matches1[0]) == 1 && count($matches2[0]) == 1) {
      $i1 = $matches1[0][0][1];
      $i2 = $matches2[0][0][1];
      $l1 = strlen($matches1[0][0][0]) + 1;
      $l2 = strlen($matches2[0][0][0]) + 1;
      if ($i2 > $i1) {
        $result['inclusion'] = trim(substr($s, $i1 + $l1, $i2 - $l1));
        $result['exclusion'] = trim(substr($s, $i2 + $l2 + 1));
      }
      else {
        $result['exclusion'] = trim(substr($s, $i2 + $l2, $i1 - $l2));
        $result['inclusion'] = trim(substr($s, $i1 + $l1 + 1));
      }
    }
    else {
      $result['inclusion'] = $s;
      $result['exclusion'] = $s;
    }
    return $result;
  }

  /**
   * Compounds value for Study Design.
   *
   * The value will aggregate three child nodes of <study_design_info>.
   *
   * @param string $study_design_info
   *   The parent XML node.
   *
   * @return string
   *    The aggregated text.
   */
  private function compoundStudyDesign($study_design_info) {
    $result = NULL;
    $result .= ($s = $study_design_info->allocation->__toString()) ? 'Allocation: ' . $s : NULL;
    $result .= ($s = $study_design_info->intervention_model->__toString()) ? '. Intervention Model: ' . $s : NULL;
    $result .= ($s = $study_design_info->primary_purpose->__toString()) ? '. Primary Purpose: ' . $s : NULL;
    $result .= ($s = $study_design_info->masking->__toString()) ? '. Masking: ' . $s : NULL;
    return $result;
  }

  /**
   * Compounds value for Trial Links.
   *
   * @param object $xml_obj
   *   The XML object.
   *
   * @return string
   *    The aggregated text.
   */
  private function compoundTrialLinks($xml_obj) {
    $result = array();
    foreach ($xml_obj->link as $item) {
      $result[] = '<a href="' .
      Xss::filter($item->url->__toString()) . '">' .
      Xss::filter($item->description->__toString()) . '</a>';
    }
    $result = implode("\r\n", $result);
    return $result;
  }

  /**
   * Compounds value for Trial References.
   *
   * @param object $xml_obj
   *   The XML object.
   *
   * @return string
   *    The aggregated text.
   */
  private function compoundTrialReferences($xml_obj) {
    $result = array();
    foreach ($xml_obj->reference as $item) {
      $result[] = '<a href="http://www.ncbi.nlm.nih.gov/pubmed/' .
        Xss::filter($item->PMID->__toString()) . '">' .
        Xss::filter($item->citation->__toString()) . '</a>';
    }
    $result = implode("\r\n", $result);
    return $result;
  }

  /**
   * Compounds value for Keywords.
   *
   * @param object $xml_obj
   *   The XML object.
   *
   * @return string
   *    The aggregated text.
   */
  private function compoundKeywords($xml_obj) {
    $result = array();
    foreach ($xml_obj->keyword as $item) {
      $result[] = $item;
    }
    $result = implode(", ", $result);
    return $result;
  }

}

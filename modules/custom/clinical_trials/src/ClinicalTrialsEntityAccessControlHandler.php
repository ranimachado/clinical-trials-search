<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\ClinicalTrialsEntityAccessControlHandler.
 */

namespace Drupal\clinical_trials;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Clinical Trials entity.
 *
 * @see \Drupal\clinical_trials\Entity\ClinicalTrialsEntity.
 */
class ClinicalTrialsEntityAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\clinical_trials\ClinicalTrialsEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished clinical trials entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published clinical trials entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit clinical trials entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete clinical trials entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add clinical trials entities');
  }

}

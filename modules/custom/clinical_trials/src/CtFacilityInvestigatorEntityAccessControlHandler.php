<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\CtFacilityInvestigatorEntityAccessControlHandler.
 */

namespace Drupal\clinical_trials;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the CT Facility Investigator entity.
 *
 * @see \Drupal\clinical_trials\Entity\CtFacilityInvestigatorEntity.
 */
class CtFacilityInvestigatorEntityAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\clinical_trials\CtFacilityInvestigatorEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished ct facility investigator entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published ct facility investigator entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit ct facility investigator entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete ct facility investigator entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add ct facility investigator entities');
  }

}

<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Entity\CtCollaboratorSponsorEntity.
 */

namespace Drupal\clinical_trials\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Collaborator Sponsor entities.
 */
class CtCollaboratorSponsorEntityViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['ct_collaborator_sponsor_entity']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Collaborator Sponsor'),
      'help' => $this->t('The Collaborator Sponsor ID.'),
    );

    return $data;
  }

}

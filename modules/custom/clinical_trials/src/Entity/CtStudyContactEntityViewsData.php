<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Entity\CtStudyContactEntity.
 */

namespace Drupal\clinical_trials\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for CT Study Contact entities.
 */
class CtStudyContactEntityViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['ct_study_contact_entity']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('CT Study Contact'),
      'help' => $this->t('The CT Study Contact ID.'),
    );

    return $data;
  }

}

<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Entity\ClinicalTrialsEntity.
 */

namespace Drupal\clinical_trials\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\clinical_trials\ClinicalTrialsEntityInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Clinical Trials entity.
 *
 * @ingroup clinical_trials
 *
 * @ContentEntityType(
 *   id = "clinical_trials_entity",
 *   label = @Translation("Clinical Trials"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\clinical_trials\ClinicalTrialsEntityListBuilder",
 *     "views_data" = "Drupal\clinical_trials\Entity\ClinicalTrialsEntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\clinical_trials\Form\ClinicalTrialsEntityForm",
 *       "add" = "Drupal\clinical_trials\Form\ClinicalTrialsEntityForm",
 *       "edit" = "Drupal\clinical_trials\Form\ClinicalTrialsEntityForm",
 *       "delete" = "Drupal\clinical_trials\Form\ClinicalTrialsEntityDeleteForm",
 *     },
 *     "access" = "Drupal\clinical_trials\ClinicalTrialsEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\clinical_trials\ClinicalTrialsEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "clinical_trials_entity",
 *   admin_permission = "administer clinical trials entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/clinical_trials_entity/{clinical_trials_entity}",
 *     "add-form" = "/admin/structure/clinical_trials_entity/add",
 *     "edit-form" = "/admin/structure/clinical_trials_entity/{clinical_trials_entity}/edit",
 *     "delete-form" = "/admin/structure/clinical_trials_entity/{clinical_trials_entity}/delete",
 *     "collection" = "/admin/structure/clinical_trials_entity",
 *   },
 *   field_ui_base_route = "clinical_trials_entity.settings"
 * )
 */
class ClinicalTrialsEntity extends ContentEntityBase implements ClinicalTrialsEntityInterface {
  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  public function getSummary() {
    return $this->get('body')->summary;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Clinical Trials entity.'))
      ->setReadOnly(TRUE);
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Clinical Trials entity.'))
      ->setReadOnly(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Clinical Trials entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the Clinical Trials entity.'))
      ->setSettings(array(
        'max_length' => 255,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['body'] = BaseFieldDefinition::create('text_with_summary')
      ->setLabel(t('Body'))
      ->setDescription(t('Body.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'text_default',
        'weight' => 10,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'text_textfield',
        'weight' => 10,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['gov_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('ClinicalTrials.gov ID'))
      ->setDescription(t('ClinicalTrials.gov ID.'))
      ->setSettings(array(
        'max_length' => 15,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 12,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 12,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['featured_trial'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Is this trial featured?'))
      ->setDescription(t('A boolean indicating whether the trial is featured?.'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'checkbox',
        'weight' => 14,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'checkbox',
        'weight' => 14,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['central_contact_info'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Central Contact Information'))
      ->setDescription(t('Central Contact Information.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 16,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 16,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['no_auto_updates'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('No automatic updates'))
      ->setDescription(t('A boolean indicating whether the trial should have automatic updates.'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'checkbox',
        'weight' => 18,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'checkbox',
        'weight' => 18,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['last_update_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Last Updated'))
      ->setDescription(t('Last update date.'))
      ->setDefaultValue(NULL)
      ->setSetting('datetime_type', 'date')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 20,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'datetime',
        'weight' => 20,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['overall_status'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Overall Status'))
      ->setDescription(t('Overall Status.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler_settings', ['target_bundles' => ['taxonomy_term' => 'overall_status']])
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 22,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'options_select',
        'weight' => 22,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'overall_status',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['lead_sponsor_agency_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Lead Sponsor Agency'))
      ->setDescription(t('Lead Sponsor Agency.'))
      ->setSettings(array(
        'max_length' => 120,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 24,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 24,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['lead_sponsor_agency_class'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Lead Sponsor Agency Class'))
      ->setDescription(t('Lead Sponsor Agency Class.'))
      ->setSettings(array(
        'max_length' => 60,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 26,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 26,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['official_title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Official Title'))
      ->setDescription(t('Official Title.'))
      ->setSettings(array(
        'max_length' => 255,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 28,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 28,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['pq_min_age'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Minimum Age'))
      ->setDescription(t('Minimum Age.'))
      ->setSettings(array(
        'max_length' => 10,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 30,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 30,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['pq_max_age'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Maximum Age'))
      ->setDescription(t('Maximum Age.'))
      ->setSettings(array(
        'max_length' => 10,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 32,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 32,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['pq_gender'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Gender'))
      ->setDescription(t('Gender.'))
      ->setSettings(array(
        'max_length' => 10,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 34,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 34,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['pq_healthy_volunteers'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Healthy Volunteers'))
      ->setDescription(t('Healthy Volunteers.'))
      ->setSettings(array(
        'max_length' => 40,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 36,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 36,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['start_date'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Study Start Date'))
      ->setDescription(t('Study Start Date.'))
      ->setSettings(array(
        'max_length' => 30,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 38,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 38,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['end_date'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Study End Date'))
      ->setDescription(t('Study End Date.'))
      ->setSettings(array(
        'max_length' => 30,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 40,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 40,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // @TODO: Remove disease_stage. But check first, maybe it's inputted manually.
    $fields['disease_stage'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Disease Stage'))
      ->setDescription(t('Disease Stage.'))
      ->setSettings(array(
        'max_length' => 255,
      ))
      ->setDefaultValue('');

    $fields['inclusion_criteria'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Inclusion Criteria'))
      ->setDescription(t('Inclusion Criteria.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'text_default',
        'weight' => 42,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'text_textfield',
        'weight' => 42,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['exclusion_criteria'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Exclusion Criteria'))
      ->setDescription(t('Exclusion Criteria.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'text_default',
        'weight' => 44,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'text_textfield',
        'weight' => 44,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['phase'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Phase'))
      ->setDescription(t('Phase.'))
      ->setSettings(array(
        'max_length' => 60,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 46,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 46,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['study_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Study Type'))
      ->setDescription(t('Study Type.'))
      ->setSettings(array(
        'max_length' => 60,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 48,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 48,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['study_design'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Study Design'))
      ->setDescription(t('Study Design.'))
      ->setSettings(array(
        'max_length' => 255,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 50,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 50,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['enrollment'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Enrollment'))
      ->setDescription(t('Enrollment'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 52,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 52,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description_adear_brief'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('ADEAR Brief Description'))
      ->setDescription(t('ADEAR Brief Description.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'text_default',
        'weight' => 54,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'text_textfield',
        'weight' => 54,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description_adear_detailed'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('ADEAR Detailed Description'))
      ->setDescription(t('ADEAR Detailed Description.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'text_default',
        'weight' => 56,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'text_textfield',
        'weight' => 56,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['conditions'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Conditions'))
      ->setDescription(t('Conditions.'))
      ->setSettings(array(
        'max_length' => 120,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 58,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 58,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // @TODO: Make it text_long.
    $fields['trial_link'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Trial Links'))
      ->setDescription(t('Trial Links.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 62,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 62,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // @TODO: Make it text_long.
    $fields['trial_ref'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Trial References'))
      ->setDescription(t('Trial References.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 63,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 63,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['keywords'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Keywords'))
      ->setDescription(t('Keywords.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 64,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 64,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // @TODO: Is this field redundant?
    $fields['criteria'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Criteria'))
      ->setDescription(t('Criteria.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 66,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 66,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['brief_description'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Brief Description'))
      ->setDescription(t('Brief Description.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 68,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 68,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['national'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('National'))
      ->setDescription(t('A boolean indicating whether the trial is national.'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'checkbox',
        'weight' => 70,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'checkbox',
        'weight' => 70,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Clinical Trials is published.'))
      ->setDefaultValue(TRUE);

//    $fields['langcode'] = BaseFieldDefinition::create('language')
//      ->setLabel(t('Language code'))
//      ->setDescription(t('The language code for the Clinical Trials entity.'))
//      ->setDisplayOptions('form', array(
//        'type' => 'en',
//        'weight' => 10,
//      ))
//      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }


}

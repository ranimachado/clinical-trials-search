<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Entity\CtFacilityInvestigatorEntity.
 */

namespace Drupal\clinical_trials\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for CT Facility Investigator entities.
 */
class CtFacilityInvestigatorEntityViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['ct_facility_investigator_entity']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('CT Facility Investigator'),
      'help' => $this->t('The CT Facility Investigator ID.'),
    );

    return $data;
  }

}

<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Entity\ClinicalTrialsEntity.
 */

namespace Drupal\clinical_trials\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Clinical Trials entities.
 */
class ClinicalTrialsEntityViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['clinical_trials_entity']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Clinical Trials'),
      'help' => $this->t('The Clinical Trials ID.'),
    );

    return $data;
  }

}

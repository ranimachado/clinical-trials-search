<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Entity\CtCategoriesEntity.
 */

namespace Drupal\clinical_trials\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for CT Categories entities.
 */
class CtCategoriesEntityViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['ct_categories_entity']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('CT Categories'),
      'help' => $this->t('The CT Categories ID.'),
    );

    return $data;
  }

}

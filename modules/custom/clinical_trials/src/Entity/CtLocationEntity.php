<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Entity\CtLocationEntity.
 */

namespace Drupal\clinical_trials\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\clinical_trials\CtLocationEntityInterface;
use Drupal\user\UserInterface;

/**
 * Defines the CT Location entity.
 *
 * @ingroup clinical_trials
 *
 * @ContentEntityType(
 *   id = "ct_location_entity",
 *   label = @Translation("Location"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\clinical_trials\CtLocationEntityListBuilder",
 *     "views_data" = "Drupal\clinical_trials\Entity\CtLocationEntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\clinical_trials\Form\CtLocationEntityForm",
 *       "add" = "Drupal\clinical_trials\Form\CtLocationEntityForm",
 *       "edit" = "Drupal\clinical_trials\Form\CtLocationEntityForm",
 *       "delete" = "Drupal\clinical_trials\Form\CtLocationEntityDeleteForm",
 *     },
 *     "access" = "Drupal\clinical_trials\CtLocationEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\clinical_trials\CtLocationEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "ct_location_entity",
 *   admin_permission = "administer ct location entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/ct_location_entity/{ct_location_entity}",
 *     "add-form" = "/admin/structure/ct_location_entity/add",
 *     "edit-form" = "/admin/structure/ct_location_entity/{ct_location_entity}/edit",
 *     "delete-form" = "/admin/structure/ct_location_entity/{ct_location_entity}/delete",
 *     "collection" = "/admin/structure/ct_location_entity",
 *   },
 *   field_ui_base_route = "ct_location_entity.settings"
 * )
 */
class CtLocationEntity extends ContentEntityBase implements CtLocationEntityInterface {
  use EntityChangedTrait;
  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the CT Location entity.'))
      ->setReadOnly(TRUE);
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the CT Location entity.'))
      ->setReadOnly(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the CT Location entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId');

    $fields['ctid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Clinical Trial ID'))
      ->setDescription(t('The ID of the Clinical Trial.'))
      ->setReadOnly(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the CT Location entity.'))
      ->setSettings(array(
        'max_length' => 255,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['longitude'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Longitude'))
      ->setDescription('Longitude.')
      ->setSettings(array(
        'precision' => 10,
        'scale' => 7,
      ));

    $fields['latitude'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Longitude'))
      ->setDescription('Longitude.')
      ->setSettings(array(
        'precision' => 10,
        'scale' => 7,
      ));

    $fields['city'] = BaseFieldDefinition::create('string')
      ->setLabel(t('City'))
      ->setDescription(t('City.'))
      ->setSettings(array(
        'max_length' => 40,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 10,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 10,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['state'] = BaseFieldDefinition::create('string')
      ->setLabel(t('State'))
      ->setDescription(t('State.'))
      ->setSettings(array(
        'max_length' => 40,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 20,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 20,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['zip'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Zip'))
      ->setDescription(t('Zip.'))
      ->setSettings(array(
        'max_length' => 12,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 30,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 30,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['country'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Country'))
      ->setDescription(t('Country.'))
      ->setSettings(array(
        'max_length' => 15,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 40,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 40,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['location_status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Location Status'))
      ->setDescription(t('Location Status.'))
      ->setSettings(array(
        'max_length' => 30,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 50,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 50,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['primary_contact_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Primary Contact Name'))
      ->setDescription(t('Primary Contact Name.'))
      ->setSettings(array(
        'max_length' => 255,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 60,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 60,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['primary_contact_phone'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Primary Contact Phone'))
      ->setDescription(t('Primary Contact Phone.'))
      ->setSettings(array(
        'max_length' => 30,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 70,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 70,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['primary_contact_email'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Primary Contact Email'))
      ->setDescription(t('Primary Contact Email.'))
      ->setSettings(array(
        'max_length' => 60,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 80,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 80,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the CT Location is published.'))
      ->setDefaultValue(TRUE);

//    $fields['langcode'] = BaseFieldDefinition::create('language')
//      ->setLabel(t('Language code'))
//      ->setDescription(t('The language code for the CT Location entity.'))
//      ->setDisplayOptions('form', array(
//        'type' => 'language_select',
//        'weight' => 10,
//      ))
//      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}

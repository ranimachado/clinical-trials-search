<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Entity\CtCollaboratorSponsorEntity.
 */

namespace Drupal\clinical_trials\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\clinical_trials\CtCollaboratorSponsorEntityInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Collaborator Sponsor entity.
 *
 * @ingroup clinical_trials
 *
 * @ContentEntityType(
 *   id = "ct_collaborator_sponsor_entity",
 *   label = @Translation("Collaborator Sponsor"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\clinical_trials\CtCollaboratorSponsorEntityListBuilder",
 *     "views_data" = "Drupal\clinical_trials\Entity\CtCollaboratorSponsorEntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\clinical_trials\Form\CtCollaboratorSponsorEntityForm",
 *       "add" = "Drupal\clinical_trials\Form\CtCollaboratorSponsorEntityForm",
 *       "edit" = "Drupal\clinical_trials\Form\CtCollaboratorSponsorEntityForm",
 *       "delete" = "Drupal\clinical_trials\Form\CtCollaboratorSponsorEntityDeleteForm",
 *     },
 *     "access" = "Drupal\clinical_trials\CtCollaboratorSponsorEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\clinical_trials\CtCollaboratorSponsorEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "ct_collaborator_sponsor_entity",
 *   admin_permission = "administer collaborator sponsor entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/ct_collaborator_sponsor_entity/{ct_collaborator_sponsor_entity}",
 *     "add-form" = "/admin/structure/ct_collaborator_sponsor_entity/add",
 *     "edit-form" = "/admin/structure/ct_collaborator_sponsor_entity/{ct_collaborator_sponsor_entity}/edit",
 *     "delete-form" = "/admin/structure/ct_collaborator_sponsor_entity/{ct_collaborator_sponsor_entity}/delete",
 *     "collection" = "/admin/structure/ct_collaborator_sponsor_entity",
 *   },
 *   field_ui_base_route = "ct_collaborator_sponsor_entity.settings"
 * )
 */
class CtCollaboratorSponsorEntity extends ContentEntityBase implements CtCollaboratorSponsorEntityInterface {
  use EntityChangedTrait;
  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Collaborator Sponsor entity.'))
      ->setReadOnly(TRUE);
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Collaborator Sponsor entity.'))
      ->setReadOnly(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Collaborator Sponsor entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId');

    $fields['ctid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Clinical Trial ID'))
      ->setDescription(t('The ID of the Clinical Trial.'))
      ->setReadOnly(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Agency'))
      ->setDescription(t('The Agency of the Collaborator Sponsor entity.'))
      ->setSettings(array(
        'max_length' => 120,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['class'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Class'))
      ->setDescription(t('The Agency Class of the Collaborator Sponsor entity.'))
      ->setSettings(array(
        'max_length' => 20,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 10,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 10,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Collaborator Sponsor is published.'))
      ->setDefaultValue(TRUE);

//    $fields['langcode'] = BaseFieldDefinition::create('language')
//      ->setLabel(t('Language code'))
//      ->setDescription(t('The language code for the Collaborator Sponsor entity.'))
//      ->setDisplayOptions('form', array(
//        'type' => 'language_select',
//        'weight' => 10,
//      ))
//      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}

<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Entity\CtInterventionEntity.
 */

namespace Drupal\clinical_trials\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\clinical_trials\CtInterventionEntityInterface;
use Drupal\user\UserInterface;

/**
 * Defines the CT Intervention entity.
 *
 * @ingroup clinical_trials
 *
 * @ContentEntityType(
 *   id = "ct_intervention_entity",
 *   label = @Translation("Intervention"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\clinical_trials\CtInterventionEntityListBuilder",
 *     "views_data" = "Drupal\clinical_trials\Entity\CtInterventionEntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\clinical_trials\Form\CtInterventionEntityForm",
 *       "add" = "Drupal\clinical_trials\Form\CtInterventionEntityForm",
 *       "edit" = "Drupal\clinical_trials\Form\CtInterventionEntityForm",
 *       "delete" = "Drupal\clinical_trials\Form\CtInterventionEntityDeleteForm",
 *     },
 *     "access" = "Drupal\clinical_trials\CtInterventionEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\clinical_trials\CtInterventionEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "ct_intervention_entity",
 *   admin_permission = "administer ct intervention entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/ct_intervention_entity/{ct_intervention_entity}",
 *     "add-form" = "/admin/structure/ct_intervention_entity/add",
 *     "edit-form" = "/admin/structure/ct_intervention_entity/{ct_intervention_entity}/edit",
 *     "delete-form" = "/admin/structure/ct_intervention_entity/{ct_intervention_entity}/delete",
 *     "collection" = "/admin/structure/ct_intervention_entity",
 *   },
 *   field_ui_base_route = "ct_intervention_entity.settings"
 * )
 */
class CtInterventionEntity extends ContentEntityBase implements CtInterventionEntityInterface {
  use EntityChangedTrait;
  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the CT Intervention entity.'))
      ->setReadOnly(TRUE);
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the CT Intervention entity.'))
      ->setReadOnly(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the CT Intervention entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId');

    $fields['ctid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Clinical Trial ID'))
      ->setDescription(t('The ID of the Clinical Trial.'))
      ->setReadOnly(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the CT Intervention entity.'))
      ->setSettings(array(
        'max_length' => 80,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Description'))
      ->setDescription(t('Description.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 10,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 10,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['arm_group_label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('ARM Group Label'))
      ->setDescription(t('ARM Group Label.'))
      ->setSettings(array(
        'max_length' => 80,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 20,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 20,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // @TODO: Is this being used?
    $fields['other_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Other Name'))
      ->setDescription(t('Other Name.'))
      ->setSettings(array(
        'max_length' => 60,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 30,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 30,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the CT Intervention is published.'))
      ->setDefaultValue(TRUE);

//    $fields['langcode'] = BaseFieldDefinition::create('language')
//      ->setLabel(t('Language code'))
//      ->setDescription(t('The language code for the CT Intervention entity.'))
//      ->setDisplayOptions('form', array(
//        'type' => 'language_select',
//        'weight' => 10,
//      ))
//      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}

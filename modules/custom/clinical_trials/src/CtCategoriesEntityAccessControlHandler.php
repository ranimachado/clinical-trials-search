<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\CtCategoriesEntityAccessControlHandler.
 */

namespace Drupal\clinical_trials;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the CT Categories entity.
 *
 * @see \Drupal\clinical_trials\Entity\CtCategoriesEntity.
 */
class CtCategoriesEntityAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\clinical_trials\CtCategoriesEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished ct categories entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published ct categories entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit ct categories entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete ct categories entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add ct categories entities');
  }

}

<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\CtInterventionEntityAccessControlHandler.
 */

namespace Drupal\clinical_trials;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the CT Intervention entity.
 *
 * @see \Drupal\clinical_trials\Entity\CtInterventionEntity.
 */
class CtInterventionEntityAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\clinical_trials\CtInterventionEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished ct intervention entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published ct intervention entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit ct intervention entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete ct intervention entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add ct intervention entities');
  }

}

<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\ClinicalTrialsUpdateService.
 */

namespace Drupal\clinical_trials;

use Drupal\Component\Utility\Xss;
use Drupal\clinical_trials\Entity\ClinicalTrialsEntity;
use Drupal\clinical_trials\Entity\CtLocationEntity;

/**
 * Class ClinicalTrialsUpdateService.
 *
 * @package Drupal\clinical_trials
 */
class ClinicalTrialsUpdateService {

  /**
   * Main method for the updating service.
   *
   * @param bool $is_cron
   *   Indicates if it is running by Cron.
   */
  public function updateAllClinicalTrials($is_cron = FALSE) {
    $cts = $this->getAllCts();
    $number_of_updates = $this->checkAndUpdate($cts);
    if ($number_of_updates == 1) {
      $message = t('%total Clinical Trial was updated.', array('%total' => $number_of_updates));
    }
    else {
      $message = t('%total Clinical Trial were updated.', array('%total' => $number_of_updates));
    }
    if ($is_cron) {
      \Drupal::logger('clinical_trials')->notice($message);
    }
    else {
      drupal_set_message($message, 'status');
    }
    $config = \Drupal::service('config.factory')->getEditable('clinical_trials.settings');
    $config->set('ct_data_importing_last_execution', time());
    $config->save();
  }

  /**
   * Gathers all CTs marked to be updated.
   *
   * @return array
   *   An array with IDs and date of all CTS.
   */
  private function getAllCts() {
    $query = \Drupal::database()->select('clinical_trials_entity', 'ct');
    $query->addField('ct', 'id');
    $query->addField('ct', 'gov_id');
    $query->addField('ct', 'last_update_date');
    $query->condition('ct.no_auto_updates', FALSE);
    $result = $query->execute()->fetchAll();
    return $result;
  }

  /**
   * Scan the CTs array and perform the update.
   *
   * Last changed date is pulled from clinicaltrials.gov is compared to the ones
   * in the respective records in the database.
   * If different, updates the record.
   *
   * @param array $cts
   *   The IDs and date of all CTS.
   *
   * @return int
   *   The number of CTs updated.
   */
  private function checkAndUpdate($cts) {
    $result = 0;
    foreach ($cts as $ct) {
      if ($result > 0) break; // @TODO: Remove this line.
      if ($xml_obj = \Drupal::service('clinical_trials.import')->getCtFromClinicalTrialsDotGov($ct->gov_id)) {
        $gov_date = strtotime($xml_obj->lastchanged_date->__toString());
        $db_date = strtotime($ct->last_update_date);
        if ($gov_date != $db_date) {
          $values = array(
            'last_update_date' => date('Y-m-d', strtotime($xml_obj->lastchanged_date->__toString())) . 'T00:00:00',
            'overall_status' => \Drupal::service('clinical_trials.import')->getOverallStatus($xml_obj->overall_status->__toString()),
            'locations' => \Drupal::service('clinical_trials.import')->extractCtLocationValuesFromXml($xml_obj, $ct->id),
          );
          if ($this->updateCt($ct->id, $values)) {
            $result++;
          }
        }
      }
    }
    return $result;
  }

  /**
   * Update the Clinical Trial entity.
   *
   * @param int $ctid
   *   The CT id.
   * @param array $values
   *   The array with the values to be saved.
   *
   * @return bool
   *   Whether the entity was properly saved.
   */
  private function updateCt($ctid, $values) {
    $result = FALSE;
    if ($entity = ClinicalTrialsEntity::load($ctid)) {
      $entity->set('last_update_date', Xss::filter($values['last_update_date']));
      $entity->set('overall_status', Xss::filter($values['overall_status']));
      if ($entity->save()) {
        if ($this->resetLocations($ctid)) {
          foreach ($values['locations'] as $values) {
            \Drupal::service('clinical_trials.import')->prepareValuesForSaving('ct_location_entity', $values);
            $entity = CtLocationEntity::create($values);
            $entity->save();
          }
          $result = TRUE;
        }
      }
    }
    return $result;
  }

  /**
   * Delete all locations ralated to a Clinical Trial.
   *
   * @param int $ctid
   *   The CT id.
   *
   * @return mixed
   *   A flag indicating whether the locations were saved.
   */
  private function resetLocations($ctid) {
    $query = \Drupal::database()->delete('ct_location_entity');
    $query->condition('ctid', $ctid);
    $result = $query->execute();
    return $result;
  }

}

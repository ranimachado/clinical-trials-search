<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\CtLocationEntityInterface.
 */

namespace Drupal\clinical_trials;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining CT Location entities.
 *
 * @ingroup clinical_trials
 */
interface CtLocationEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the CT Location name.
   *
   * @return string
   *   Name of the CT Location.
   */
  public function getName();

  /**
   * Sets the CT Location name.
   *
   * @param string $name
   *   The CT Location name.
   *
   * @return \Drupal\clinical_trials\CtLocationEntityInterface
   *   The called CT Location entity.
   */
  public function setName($name);

  /**
   * Gets the CT Location creation timestamp.
   *
   * @return int
   *   Creation timestamp of the CT Location.
   */
  public function getCreatedTime();

  /**
   * Sets the CT Location creation timestamp.
   *
   * @param int $timestamp
   *   The CT Location creation timestamp.
   *
   * @return \Drupal\clinical_trials\CtLocationEntityInterface
   *   The called CT Location entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the CT Location published status indicator.
   *
   * Unpublished CT Location are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the CT Location is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a CT Location.
   *
   * @param bool $published
   *   TRUE to set this CT Location to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\clinical_trials\CtLocationEntityInterface
   *   The called CT Location entity.
   */
  public function setPublished($published);

}

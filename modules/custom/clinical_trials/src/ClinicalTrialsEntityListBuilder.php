<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\ClinicalTrialsEntityListBuilder.
 */

namespace Drupal\clinical_trials;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Clinical Trials entities.
 *
 * @ingroup clinical_trials
 */
class ClinicalTrialsEntityListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Clinical Trials ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\clinical_trials\Entity\ClinicalTrialsEntity */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.clinical_trials_entity.edit_form', array(
          'clinical_trials_entity' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}

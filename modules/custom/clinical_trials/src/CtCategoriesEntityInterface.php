<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\CtCategoriesEntityInterface.
 */

namespace Drupal\clinical_trials;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining CT Categories entities.
 *
 * @ingroup clinical_trials
 */
interface CtCategoriesEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the CT Categories name.
   *
   * @return string
   *   Name of the CT Categories.
   */
  public function getName();

  /**
   * Sets the CT Categories name.
   *
   * @param string $name
   *   The CT Categories name.
   *
   * @return \Drupal\clinical_trials\CtCategoriesEntityInterface
   *   The called CT Categories entity.
   */
  public function setName($name);

  /**
   * Gets the CT Categories creation timestamp.
   *
   * @return int
   *   Creation timestamp of the CT Categories.
   */
  public function getCreatedTime();

  /**
   * Sets the CT Categories creation timestamp.
   *
   * @param int $timestamp
   *   The CT Categories creation timestamp.
   *
   * @return \Drupal\clinical_trials\CtCategoriesEntityInterface
   *   The called CT Categories entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the CT Categories published status indicator.
   *
   * Unpublished CT Categories are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the CT Categories is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a CT Categories.
   *
   * @param bool $published
   *   TRUE to set this CT Categories to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\clinical_trials\CtCategoriesEntityInterface
   *   The called CT Categories entity.
   */
  public function setPublished($published);

}

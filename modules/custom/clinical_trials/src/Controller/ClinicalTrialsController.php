<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Controller\ClinicalTrialsController.
 */

namespace Drupal\clinical_trials\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\clinical_trials\Entity\ClinicalTrialsEntity;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Class ClinicalTrialsController.
 *
 * @package Drupal\clinical_trials\Controller
 */
class ClinicalTrialsController extends ControllerBase {

  private $clinicalTrialsEntity = NULL;

  private $notRelevantFields = array(
    'id',
    'uuid',
    'langcode',
    'user_id',
    'ctid',
    'status',
    'created',
    'changed',
  );

  /**
   * Hello.
   *
   * @return string
   *   Return Hello string.
   */
  public function content($id) {
    if ($this->clinicalTrialsEntity = ClinicalTrialsEntity::load($id)) {
      $content = $this->buildContent();
      $content['has_content'] = TRUE;
    }
    else {
      $content['name']['value'] = t("ERROR: Clinical Trial doesn't exist: %id", array('%id' => $id));
      $content['has_content'] = FALSE;

    }
    return array(
      '#theme' => 'clinical_trials_page',
      '#content' => $content,
    );
  }

  /**
   * Builds the associative array of field names => values to be displayed.
   *
   * @return array $content
   *   The array of values.
   */
  private function buildContent() {
    $content = array();
    $user = \Drupal::currentUser();
    if ($user->hasPermission('edit clinical trials entities')) {
      $id = $this->clinicalTrialsEntity->id();
      $edit_url = Url::fromRoute('entity.clinical_trials_entity.edit_form', ['clinical_trials_entity' => $id]);
      $content['edit_link'] = Link::fromTextAndUrl(t('Edit'), $edit_url)->toString();
    }
    else {
      $content['edit_link'] = NULL;
    }
    if ($user->hasPermission('delete clinical trials entities')) {
      $id = $this->clinicalTrialsEntity->id();
      $edit_url = Url::fromRoute('entity.clinical_trials_entity.delete_form', ['clinical_trials_entity' => $id]);
      $content['delete_link'] = Link::fromTextAndUrl(t('Delete'), $edit_url)->toString();
    }
    else {
      $content['delete_link'] = NULL;
    }
    $fields = array_keys($this->clinicalTrialsEntity->getFieldDefinitions());
    $fields = array_diff($fields, $this->notRelevantFields);
    foreach ($fields as $field) {
      $content[$field] = $this->getClinicalTrialsValue($field);
    }
    $content = array_merge($content, \Drupal::service('clinical_trials.get_data')->getAllAggregatedData($this->clinicalTrialsEntity));
    return $content;
  }

  /**
   * Extracts the value of a given field.
   *
   * @param string $field_name
   *   The name of the field.
   * @param int $key
   *   The index of the field value. Default 0.
   *
   * @return mixed
   *    The field value.
   */
  private function getClinicalTrialsValue($field_name, $key = 0) {
    $value = $this->clinicalTrialsEntity->get($field_name)->getValue();
    $result = $value[$key];
    $result['item_id'] = 'ct-field-' . str_replace('_', '-', $field_name);
    return $result;
  }

}

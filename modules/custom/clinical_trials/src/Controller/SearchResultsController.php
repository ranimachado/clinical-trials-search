<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Controller\SearchResultsController.
 */

namespace Drupal\clinical_trials\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\search_api\Entity\Index;
use Drupal\clinical_trials\Entity\ClinicalTrialsEntity;

/**
 * Class SearchResultsController.
 *
 * @package Drupal\clinical_trials\Controller
 */
class SearchResultsController extends ControllerBase {
  /**
   * Content.
   *
   * @return string
   *   Return Hello string.
   */
  public function content($term) {

    $query = Index::load('clinical_trials_index')->query();
    $query->keys($term);
    $query->range(0, 25);

    if ($data = $query->execute()) {
      $results = $this->formatResults($data);
    }
    else {
      $results = $this->t('No results found!');
    }

    return [
      '#theme' => 'clinical_trials_search_results',
      '#results'=> $results,
    ];
  }

  /**
   * Format the results returned by the query execution.
   *
   * @param object $data
   *   Results object.
   *
   * @return array
   *   Formatted results.
   */
  private function formatResults($data) {
    $result = [];
    $extra_data = $data->getExtraData('search_api_solr_response');
    foreach ($extra_data['highlighting'] as $extra_data_key => $extra_data_value) {
      $extra_data_key = explode('/', $extra_data_key);
      $extra_data_key = explode(':', $extra_data_key[1]);
      $entity_id = $extra_data_key[0];
      $extra_data_spell = $extra_data_value['spell'];
      foreach ($extra_data_value['spell'] as $key => $spell) {
        foreach (@$extra_data_value['ts_name'] as $ts_name) {
          if (strpos($ts_name, $spell) !== FALSE) {
            unset($extra_data_spell[$key]);
          }
        }
      }
      $excerpt = implode('... ', $extra_data_spell);
      $excerpt = str_replace('[HIGHLIGHT]', '<strong>', $excerpt);
      $excerpt = str_replace('[/HIGHLIGHT]', '</strong>', $excerpt);
      if (!array_key_exists($entity_id, $result) && $entity = ClinicalTrialsEntity::load($entity_id)) {
        $result[$entity_id] = [
          'title' => '<a href="/clinical-trials/' . $entity_id . '">' . $entity->getName() . '</a>',
          'summary' => $entity->getSummary(),
          'excerpt' => $excerpt,
        ];
      }
    }
    return $result;
  }

}

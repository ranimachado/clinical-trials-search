<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\CtCollaboratorSponsorEntityInterface.
 */

namespace Drupal\clinical_trials;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Collaborator Sponsor entities.
 *
 * @ingroup clinical_trials
 */
interface CtCollaboratorSponsorEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Collaborator Sponsor name.
   *
   * @return string
   *   Name of the Collaborator Sponsor.
   */
  public function getName();

  /**
   * Sets the Collaborator Sponsor name.
   *
   * @param string $name
   *   The Collaborator Sponsor name.
   *
   * @return \Drupal\clinical_trials\CtCollaboratorSponsorEntityInterface
   *   The called Collaborator Sponsor entity.
   */
  public function setName($name);

  /**
   * Gets the Collaborator Sponsor creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Collaborator Sponsor.
   */
  public function getCreatedTime();

  /**
   * Sets the Collaborator Sponsor creation timestamp.
   *
   * @param int $timestamp
   *   The Collaborator Sponsor creation timestamp.
   *
   * @return \Drupal\clinical_trials\CtCollaboratorSponsorEntityInterface
   *   The called Collaborator Sponsor entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Collaborator Sponsor published status indicator.
   *
   * Unpublished Collaborator Sponsor are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Collaborator Sponsor is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Collaborator Sponsor.
   *
   * @param bool $published
   *   TRUE to set this Collaborator Sponsor to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\clinical_trials\CtCollaboratorSponsorEntityInterface
   *   The called Collaborator Sponsor entity.
   */
  public function setPublished($published);

}

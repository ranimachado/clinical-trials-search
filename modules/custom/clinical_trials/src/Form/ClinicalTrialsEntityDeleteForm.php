<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Form\ClinicalTrialsEntityDeleteForm.
 */

namespace Drupal\clinical_trials\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Clinical Trials entities.
 *
 * @ingroup clinical_trials
 */
class ClinicalTrialsEntityDeleteForm extends ContentEntityDeleteForm {

}

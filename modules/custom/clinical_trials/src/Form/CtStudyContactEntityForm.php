<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Form\CtStudyContactEntityForm.
 */

namespace Drupal\clinical_trials\Form;

use Drupal\Core\Entity\ContentEntityForm;

/**
 * Form controller for CT Study Contact edit forms.
 *
 * @ingroup clinical_trials
 */
class CtStudyContactEntityForm extends ContentEntityForm {

  use AggregatedEntityForm;

}

<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Form\CtLocationEntityForm.
 */

namespace Drupal\clinical_trials\Form;

use Drupal\Core\Entity\ContentEntityForm;

/**
 * Form controller for CT Location edit forms.
 *
 * @ingroup clinical_trials
 */
class CtLocationEntityForm extends ContentEntityForm {

  use AggregatedEntityForm;

}

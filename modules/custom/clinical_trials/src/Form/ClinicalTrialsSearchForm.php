<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Form\ClinicalTrialsSearchForm.
 */

namespace Drupal\clinical_trials\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ClinicalTrialsSearchForm.
 *
 * @package Drupal\clinical_trials\Form
 */
class ClinicalTrialsSearchForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'clinical_trials_search_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['search_term'] = array(
      '#type' => 'search',
      '#title' => $this->t('Search Term'),
    );
    $form['search'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Search'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('clinical_trials.search_results', ['term' => $form_state->getValue('search_term')]);
  }

}

<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Form\CtLocationEntityDeleteForm.
 */

namespace Drupal\clinical_trials\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting CT Location entities.
 *
 * @ingroup clinical_trials
 */
class CtLocationEntityDeleteForm extends ContentEntityDeleteForm {

  use AggregatedEntityDeleteForm;

}

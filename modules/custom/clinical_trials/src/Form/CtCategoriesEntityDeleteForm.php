<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Form\CtCategoriesEntityDeleteForm.
 */

namespace Drupal\clinical_trials\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting CT Categories entities.
 *
 * @ingroup clinical_trials
 */
class CtCategoriesEntityDeleteForm extends ContentEntityDeleteForm {

}

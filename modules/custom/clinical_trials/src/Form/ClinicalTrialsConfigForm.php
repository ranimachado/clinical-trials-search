<?php

/**
 * @file
 * Contains Drupal\clinical_trials\Form\ClinicalTrialsConfigForm.
 */

namespace Drupal\clinical_trials\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ClinicalTrialsConfigForm.
 *
 * @package Drupal\clinical_trials\Form
 */
class ClinicalTrialsConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'clinical_trials.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'clinical_trials_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('clinical_trials.settings');

    $form['clinicaltrial_gov_api_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t("ClinicalTrial.gov API's URL."),
      '#default_value' => $config->get('clinicaltrial_gov_api_url'),
      '#description' => $this->t('Using "%s" as the placeholder for the CT id.'),
    );

    $form['ct_data_importing_interval'] = array(
      '#type' => 'select',
      '#title' => $this->t('Clinical Trials updating interval'),
      '#description' => $this->t('Sets the interval between the execution of two Clinical Trials updates triggered by Cron.'),
      '#options' => array(
        0 => $this->t('Every Cron Job'),
        60 * 5 => $this->t('5 minutes - for development purposes'),
        60 * 10 => $this->t('10 minutes - for development purposes'),
        60 * 60 => $this->t('1 hour'),
        60 * 60 * 2 => $this->t('2 hours'),
        60 * 60 * 3 => $this->t('3 hours'),
        60 * 60 * 4 => $this->t('4 hours'),
        60 * 60 * 5 => $this->t('5 hours'),
        60 * 60 * 6 => $this->t('6 hours'),
        60 * 60 * 12 => $this->t('12 hours'),
        60 * 60 * 18 => $this->t('18 hours'),
        60 * 60 * 24 => $this->t('24 hours'),
        60 * 60 * 24 * 2 => $this->t('2 days'),
        60 * 60 * 24 * 3 => $this->t('3 days'),
        60 * 60 * 24 * 4 => $this->t('4 days'),
        60 * 60 * 24 * 5 => $this->t('5 days'),
        60 * 60 * 24 * 6 => $this->t('6 days'),
        60 * 60 * 24 * 7 => $this->t('1 week'),
        60 * 60 * 24 * 7 * 2 => $this->t('2 weeks'),
        60 * 60 * 24 * 7 * 3 => $this->t('3 weeks'),
        60 * 60 * 24 * 7 * 4 => $this->t('1 month'),
      ),
      '#default_value' => $config->get('ct_data_importing_interval'),
    );
    
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('clinical_trials.settings')
      ->set('clinicaltrial_gov_api_url', $form_state->getValue('clinicaltrial_gov_api_url'))
      ->set('ct_data_importing_interval', $form_state->getValue('ct_data_importing_interval'))
      ->save();
  }

}

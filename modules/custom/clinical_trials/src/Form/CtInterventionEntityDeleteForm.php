<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Form\CtInterventionEntityDeleteForm.
 */

namespace Drupal\clinical_trials\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting CT Intervention entities.
 *
 * @ingroup clinical_trials
 */
class CtInterventionEntityDeleteForm extends ContentEntityDeleteForm {

  use AggregatedEntityDeleteForm;

}

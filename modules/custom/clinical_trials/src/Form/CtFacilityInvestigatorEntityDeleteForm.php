<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Form\CtFacilityInvestigatorEntityDeleteForm.
 */

namespace Drupal\clinical_trials\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting CT Facility Investigator entities.
 *
 * @ingroup clinical_trials
 */
class CtFacilityInvestigatorEntityDeleteForm extends ContentEntityDeleteForm {

  use AggregatedEntityDeleteForm;

}

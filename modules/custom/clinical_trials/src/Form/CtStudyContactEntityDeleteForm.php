<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Form\CtStudyContactEntityDeleteForm.
 */

namespace Drupal\clinical_trials\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting CT Study Contact entities.
 *
 * @ingroup clinical_trials
 */
class CtStudyContactEntityDeleteForm extends ContentEntityDeleteForm {

  use AggregatedEntityDeleteForm;

}

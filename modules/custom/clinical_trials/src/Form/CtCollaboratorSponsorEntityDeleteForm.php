<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Form\CtCollaboratorSponsorEntityDeleteForm.
 */

namespace Drupal\clinical_trials\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting CT aggregated entities.
 *
 * @ingroup clinical_trials
 */
class CtCollaboratorSponsorEntityDeleteForm extends ContentEntityDeleteForm {

  use AggregatedEntityDeleteForm;

}

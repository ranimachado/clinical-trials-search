<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Form\ClinicalTrialsEntityForm.
 */

namespace Drupal\clinical_trials\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Component\Serialization\Json;

/**
 * Form controller for Clinical Trials edit forms.
 *
 * @ingroup clinical_trials
 */
class ClinicalTrialsEntityForm extends ContentEntityForm {
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // @TODO: Reposition criteria to right after exclusion criteria.
    /* @var $entity \Drupal\clinical_trials\Entity\ClinicalTrialsEntity */
    $form = parent::buildForm($form, $form_state);

    $this->buildAggregatedEntityElements($form, 'ct_categories_entity', 26);
    $this->buildAggregatedEntityElements($form, 'ct_collaborator_sponsor_entity', 27);
    $this->buildAggregatedEntityElements($form, 'ct_facility_investigator_entity', 29);
    $this->buildAggregatedEntityElements($form, 'ct_intervention_entity', 59);
    $this->buildAggregatedEntityElements($form, 'ct_study_contact_entity', 60);
    $this->buildAggregatedEntityElements($form, 'ct_location_entity', 61);

    return $form;
  }

  /**
   * Builds the aggregated entities elements of the form.
   *
   * @param array $form
   *   Form elements array.
   */
  private function buildAggregatedEntityElements(array &$form, $entity_type, $element_weight = 95) {

    $entity_definition = \Drupal::entityTypeManager()->getDefinition($entity_type);

    $add_route = 'entity.' . $entity_type . '.add_form';
    $edit_route = 'entity.' . $entity_type . '.edit_form';
    $delete_route = 'entity.' . $entity_type . '.delete_form';

    $form[$entity_type] = array(
      '#type' => 'details',
      '#title' => $entity_definition->getLabel()->render(),
      '#open' => TRUE,
      '#weight' => $element_weight,
    );
    $add_url = Url::fromRoute($add_route, ['ctid' => $this->entity->id()]);
    $add_url->setOptions([
      'attributes' => [
        'class' => ['use-ajax', 'button', 'button--small'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode(['width' => 400]),
      ],
    ]);
    $form[$entity_type]['operations_add'] = array(
      '#type' => 'item',
      '#markup' => Link::fromTextAndUrl(t('Add'), $add_url)->toString(),
    );

    $fields_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type, NULL);
    $field_names = array_diff(
      array_keys($fields_definitions),
      array(
        'id',
        'uuid',
        'user_id',
        'ctid',
        'status',
        'langcode',
        'created',
        'changed',
      )
    );

    $items = $this->getAggregatedEntity($entity_type, $this->entity->id());
    foreach ($items as $item) {
      $edit_url = Url::fromRoute($edit_route, [
        $entity_type => $item['id'],
        'ctid' => $this->entity->id(),
      ]);
      $edit_url->setOptions([
        'attributes' => [
          'class' => ['use-ajax', 'button', 'button--small'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode(['width' => 400]),
        ],
      ]);
      $delete_url = Url::fromRoute($delete_route, [
        $entity_type => $item['id'],
        'ctid' => $this->entity->id(),
      ]);
      $delete_url->setOptions([
        'attributes' => [
          'class' => ['use-ajax', 'button', 'button--small'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode(['width' => 400]),
        ],
      ]);
      $fieldset_id = 'fields_' . $item['id'];
      $form[$entity_type][$fieldset_id] = array(
        '#type' => 'fieldset',
        '#title' => NULL,
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
        '#attributes' => array('class' => array('ct-aggregated-subfieldset')),
      );
      $form[$entity_type][$fieldset_id]['name_' . $item['id']] = array(
        '#type' => 'item',
        '#title' => $this->t('Agency'),
        '#markup' => $item['name'],
      );

      foreach ($field_names as $field_name) {
        $form[$entity_type][$fieldset_id][$field_name . '_' . $item['id']] = array(
          '#type' => 'item',
          '#title' => $fields_definitions[$field_name]->getLabel()->render(),
          '#markup' => $item[$field_name],
        );
      }
      $form[$entity_type][$fieldset_id]['operations_' . $item['id']] = array(
        '#type' => 'item',
        '#markup' => Link::fromTextAndUrl(t('Edit'), $edit_url)->toString() . ' ' . Link::fromTextAndUrl(t('Delete'), $delete_url)->toString(),
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Clinical Trials.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Clinical Trials.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.clinical_trials_entity.canonical', ['clinical_trials_entity' => $entity->id()]);
  }

  /**
   * Get the aggregated entities data.
   *
   * @param string $entity_type
   *   The entity type.
   * @param int $ctid
   *   The entity id.
   *
   * @return array
   *   The rows of the aggregated data.
   */
  private function getAggregatedEntity($entity_type, $ctid) {
    $query = \Drupal::database()->select($entity_type, 't');
    $query->fields('t');
    $query->condition('t.ctid', $ctid);
    $query->execute();
    $data = $query->execute();
    return $data->fetchAll(\PDO::FETCH_ASSOC);
  }

}

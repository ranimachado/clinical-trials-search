<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Form\CtInterventionEntityForm.
 */

namespace Drupal\clinical_trials\Form;

use Drupal\Core\Entity\ContentEntityForm;

/**
 * Form controller for CT Intervention edit forms.
 *
 * @ingroup clinical_trials
 */
class CtInterventionEntityForm extends ContentEntityForm {

  use AggregatedEntityForm;

}

<?php
/**
 * @file
 * Contains \Drupal\clinical_trials\Form\AggregatedEntityForm.
 */

namespace Drupal\clinical_trials\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

trait AggregatedEntityForm {

  private static $labelOfTheEntity = NULL;
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\clinical_trials\Entity\CtFacilityInvestigatorEntity */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;
    $entity_definition = \Drupal::entityTypeManager()->getDefinition($entity->getEntityTypeId());
    self::$labelOfTheEntity = $entity_definition->getLabel()->render();
    if ($entity->isNew()) {
      $form['#title'] = $this->t('Add %label', array('%label' => self::$labelOfTheEntity));
    }
    else {
      $form['#title'] = $this->t('Edit %label', array('%label' => self::$labelOfTheEntity));
    }
    $form['#title'] = str_replace('<em class="placeholder">', '', $form['#title']->render());
    $form['#title'] = str_replace('</em>', '', $form['#title']);
    unset($form['actions']['delete']);
    unset($form['actions']['cancel']);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $param = \Drupal::request()->query->all();
    $ctid = $param['ctid'];
    $entity->ctid->setValue($ctid);
    $status = parent::save($form, $form_state);
    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %entity_label %record_label.', [
          '%entity_label' => self::$labelOfTheEntity,
          '%record_label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %entity_label %record_label.', [
          '%entity_label' => self::$labelOfTheEntity,
          '%record_label' => $entity->label(),
        ]));
    }
    $url = Url::fromRoute('entity.clinical_trials_entity.edit_form', ['clinical_trials_entity' => $ctid]);
    $form_state->setRedirectUrl($url);
  }

}
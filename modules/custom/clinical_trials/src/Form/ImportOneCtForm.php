<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Form\ImportOneCtForm.
 */

namespace Drupal\clinical_trials\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SubmitCtForm.
 *
 * @package Drupal\clinical_trials\Form
 */
class ImportOneCtForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'submit_ct_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['gov_ctid'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('NCT Number'),
      '#description' => $this->t('Enter the clinicaltrials.gov ID for the Trial.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#required' => TRUE,
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Import Clinical Trial',
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $gov_ctid = \Drupal\Component\Utility\Xss::filter($form_state->getValue('gov_ctid'));
    \Drupal::service('clinical_trials.import')->importClinicalTrial($gov_ctid);
  }

}

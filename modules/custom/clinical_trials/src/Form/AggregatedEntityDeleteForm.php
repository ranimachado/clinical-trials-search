<?php
/**
 * @file
 * Contains \Drupal\clinical_trials\Form\AggregatedEntityDeleteForm.
 */

namespace Drupal\clinical_trials\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

trait AggregatedEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->getEntity();

    $form['#title'] = str_replace('<em class="placeholder">', '', $form['#title']->render());
    $form['#title'] = str_replace('</em>', '', $form['#title']);
    unset($form['actions']['cancel']);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $param = \Drupal::request()->query->all();
    $ctid = $param['ctid'];
    $url = Url::fromRoute('entity.clinical_trials_entity.edit_form', ['clinical_trials_entity' => $ctid]);
    $form_state->setRedirectUrl($url);
  }

}
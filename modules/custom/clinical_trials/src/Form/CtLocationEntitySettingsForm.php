<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Form\CtLocationEntitySettingsForm.
 */

namespace Drupal\clinical_trials\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CtLocationEntitySettingsForm.
 *
 * @package Drupal\clinical_trials\Form
 *
 * @ingroup clinical_trials
 */
class CtLocationEntitySettingsForm extends FormBase {
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'CtLocationEntity_settings';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
  }


  /**
   * Defines the settings form for CT Location entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['CtLocationEntity_settings']['#markup'] = 'Settings form for CT Location entities. Manage field settings here.';
    return $form;
  }

}

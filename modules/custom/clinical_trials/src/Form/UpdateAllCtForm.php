<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Form\UpdateAllCtForm.
 */

namespace Drupal\clinical_trials\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class UpdateAllCtForm.
 *
 * @package Drupal\clinical_trials\Form
 */
class UpdateAllCtForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'update_all_ct_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['message'] = array(
      '#type' => 'item',
      '#markup' => $this->t('Please confirm the execution of the Clinical Trials updating process:'),
    );
    $form['import'] = array(
      '#type' => 'submit',
      '#title' => $this->t('Update'),
      '#value' => $this->t('Update all Clinical Trials'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::service('clinical_trials.update')->updateAllClinicalTrials();
  }

}

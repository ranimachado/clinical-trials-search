<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Form\CtCollaboratorSponsorEntityForm.
 */

namespace Drupal\clinical_trials\Form;

use Drupal\Core\Entity\ContentEntityForm;

/**
 * Form controller for CT aggregated edit forms.
 *
 * @ingroup clinical_trials
 */
class CtCollaboratorSponsorEntityForm extends ContentEntityForm {

  use AggregatedEntityForm;

}

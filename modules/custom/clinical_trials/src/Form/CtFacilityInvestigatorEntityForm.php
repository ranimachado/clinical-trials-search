<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\Form\CtFacilityInvestigatorEntityForm.
 */

namespace Drupal\clinical_trials\Form;

use Drupal\Core\Entity\ContentEntityForm;

/**
 * Form controller for CT Facility Investigator edit forms.
 *
 * @ingroup clinical_trials
 */
class CtFacilityInvestigatorEntityForm extends ContentEntityForm {

  use AggregatedEntityForm;

}

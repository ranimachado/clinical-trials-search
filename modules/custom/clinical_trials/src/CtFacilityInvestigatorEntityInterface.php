<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\CtFacilityInvestigatorEntityInterface.
 */

namespace Drupal\clinical_trials;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining CT Facility Investigator entities.
 *
 * @ingroup clinical_trials
 */
interface CtFacilityInvestigatorEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the CT Facility Investigator name.
   *
   * @return string
   *   Name of the CT Facility Investigator.
   */
  public function getName();

  /**
   * Sets the CT Facility Investigator name.
   *
   * @param string $name
   *   The CT Facility Investigator name.
   *
   * @return \Drupal\clinical_trials\CtFacilityInvestigatorEntityInterface
   *   The called CT Facility Investigator entity.
   */
  public function setName($name);

  /**
   * Gets the CT Facility Investigator creation timestamp.
   *
   * @return int
   *   Creation timestamp of the CT Facility Investigator.
   */
  public function getCreatedTime();

  /**
   * Sets the CT Facility Investigator creation timestamp.
   *
   * @param int $timestamp
   *   The CT Facility Investigator creation timestamp.
   *
   * @return \Drupal\clinical_trials\CtFacilityInvestigatorEntityInterface
   *   The called CT Facility Investigator entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the CT Facility Investigator published status indicator.
   *
   * Unpublished CT Facility Investigator are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the CT Facility Investigator is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a CT Facility Investigator.
   *
   * @param bool $published
   *   TRUE to set this CT Facility Investigator to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\clinical_trials\CtFacilityInvestigatorEntityInterface
   *   The called CT Facility Investigator entity.
   */
  public function setPublished($published);

}

<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\ClinicalTrialGetDataService.
 */

namespace Drupal\clinical_trials;

use Drupal\Core\Database\Database;
use Drupal\taxonomy\Entity\Term;

/**
 * Class ClinicalTrialGetDataService.
 *
 * @package Drupal\clinical_trials
 */
class ClinicalTrialGetDataService {

  private $notRelevantFields = array(
    'id',
    'uuid',
    'langcode',
    'user_id',
    'ctid',
    'status',
    'created',
    'changed',
  );

  /**
   * Gathers all data related to a single Clinical Trial entity.
   *
   * @param object $clinical_trial_entity
   *   The CT entity.
   *
   * @return array
   *   An aggregation of values pulled from all C
   */
  public function getAllAggregatedData($clinical_trial_entity) {
    $result = array();
    $result['collaborator_sponsors'] = $this->getAggregatedEntityData($clinical_trial_entity->id(), 'ct_collaborator_sponsor_entity');
    $result['facility_investigators'] = $this->getAggregatedEntityData($clinical_trial_entity->id(), 'ct_facility_investigator_entity');
    $result['intervention'] = $this->getAggregatedEntityData($clinical_trial_entity->id(), 'ct_intervention_entity');
    $result['study_contact'] = $this->getAggregatedEntityData($clinical_trial_entity->id(), 'ct_study_contact_entity');
    $result['location'] = $this->getAggregatedEntityData($clinical_trial_entity->id(), 'ct_location_entity');
    $result['overall_status'] = $this->getTaxonomyTerm($clinical_trial_entity->overall_status->getValue());
    $tids = $this->getTaxonomyIds($clinical_trial_entity->disease_stage->getValue());
    $result['disease_stage'] = $this->getTaxonomyTerm($tids);
    return $result;
  }

  /**
   * Finds the table name and the fields names and performs a query.
   *
   * @param string $entity_id
   *   The table id as referred in the entity anotation.
   *
   * @return array $result
   *   The resulting values for the query;
   */
  public function getAggregatedEntityData($ctid, $entity_id) {
    $entity_manager = \Drupal::service('entity.manager');
    $storage = $entity_manager->getStorage($entity_id);
    // Base table is probably the same as $entity_id, but just to be sure.
    $base_table = $storage->getBaseTable();
    $entity_field_manager = \Drupal::service('entity_field.manager');
    $fields = $entity_field_manager->getFieldDefinitions($entity_id, NULL);
    $fields = array_keys($fields);
    $fields = array_diff($fields, $this->notRelevantFields);
    $connection = Database::getConnection();
    $sth = $connection->select($base_table, 't')
      ->fields('t', $fields)
      ->condition('ctid', $ctid, '=');
    $data = $sth->execute();
    $result = $data->fetchAll(\PDO::FETCH_ASSOC);
    return $result;
  }

  /**
   * Get the description/name of vocabulary terms.
   *
   * @param array $values
   *   The list of term ids.
   *
   * @return array
   *   A list of all terms.
   */
  private function getTaxonomyTerm($values) {
    $result = array();
    foreach ($values as $value) {
      if ($term = Term::load($value['target_id'])) {
        $result[] = $term->getName();
      }
    }
    return $result;
  }

  /**
   * Extract term ids.
   *
   * @param array $values
   *   Contain a serialized array of term ids.
   *
   * @return array
   *   The list of term ids.
   */
  private function getTaxonomyIds($values) {
    $result = array();
    $tids = unserialize($values[0]['value']);
    foreach ($tids as $tid) {
      $result[] = array(
        'target_id' => $tid,
      );
    }
    return $result;
  }

}

<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\CtStudyContactEntityListBuilder.
 */

namespace Drupal\clinical_trials;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of CT Study Contact entities.
 *
 * @ingroup clinical_trials
 */
class CtStudyContactEntityListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('CT Study Contact ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\clinical_trials\Entity\CtStudyContactEntity */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.ct_study_contact_entity.edit_form', array(
          'ct_study_contact_entity' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}

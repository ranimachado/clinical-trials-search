<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\CtStudyContactEntityInterface.
 */

namespace Drupal\clinical_trials;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining CT Study Contact entities.
 *
 * @ingroup clinical_trials
 */
interface CtStudyContactEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.

  /**
   * Gets the CT Study Contact name.
   *
   * @return string
   *   Name of the CT Study Contact.
   */
  public function getName();

  /**
   * Sets the CT Study Contact name.
   *
   * @param string $name
   *   The CT Study Contact name.
   *
   * @return \Drupal\clinical_trials\CtStudyContactEntityInterface
   *   The called CT Study Contact entity.
   */
  public function setName($name);

  /**
   * Gets the CT Study Contact creation timestamp.
   *
   * @return int
   *   Creation timestamp of the CT Study Contact.
   */
  public function getCreatedTime();

  /**
   * Sets the CT Study Contact creation timestamp.
   *
   * @param int $timestamp
   *   The CT Study Contact creation timestamp.
   *
   * @return \Drupal\clinical_trials\CtStudyContactEntityInterface
   *   The called CT Study Contact entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the CT Study Contact published status indicator.
   *
   * Unpublished CT Study Contact are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the CT Study Contact is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a CT Study Contact.
   *
   * @param bool $published
   *   TRUE to set this CT Study Contact to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\clinical_trials\CtStudyContactEntityInterface
   *   The called CT Study Contact entity.
   */
  public function setPublished($published);

}

<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\CtCollaboratorSponsorEntityListBuilder.
 */

namespace Drupal\clinical_trials;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Collaborator Sponsor entities.
 *
 * @ingroup clinical_trials
 */
class CtCollaboratorSponsorEntityListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Collaborator Sponsor ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\clinical_trials\Entity\CtCollaboratorSponsorEntity */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.ct_collaborator_sponsor_entity.edit_form', array(
          'ct_collaborator_sponsor_entity' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}

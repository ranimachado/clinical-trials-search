<?php

/**
 * @file
 * Contains \Drupal\clinical_trials\CtCategoriesEntityListBuilder.
 */

namespace Drupal\clinical_trials;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of CT Categories entities.
 *
 * @ingroup clinical_trials
 */
class CtCategoriesEntityListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('CT Categories ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\clinical_trials\Entity\CtCategoriesEntity */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.ct_categories_entity.edit_form', array(
          'ct_categories_entity' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}

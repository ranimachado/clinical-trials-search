<?php

/**
 * @file
 * Contains ct_collaborator_sponsor_entity.page.inc..
 *
 * Page callback for Collaborator Sponsor entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Collaborator Sponsor templates.
 *
 * Default template: ct_collaborator_sponsor_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_ct_collaborator_sponsor_entity(array &$variables) {
  // Fetch CtCollaboratorSponsorEntity Entity Object.
  $ct_collaborator_sponsor_entity = $variables['elements']['#ct_collaborator_sponsor_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

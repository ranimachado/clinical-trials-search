<?php

/**
 * @file
 * Contains ct_study_contact_entity.page.inc..
 *
 * Page callback for CT Study Contact entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for CT Study Contact templates.
 *
 * Default template: ct_study_contact_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_ct_study_contact_entity(array &$variables) {
  // Fetch CtStudyContactEntity Entity Object.
  $ct_study_contact_entity = $variables['elements']['#ct_study_contact_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

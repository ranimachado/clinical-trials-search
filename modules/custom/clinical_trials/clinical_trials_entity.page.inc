<?php

/**
 * @file
 * Contains clinical_trials_entity.page.inc..
 *
 * Page callback for Clinical Trials entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Clinical Trials templates.
 *
 * Default template: clinical_trials_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_clinical_trials_entity(array &$variables) {
  // Fetch ClinicalTrialsEntity Entity Object.
  $clinical_trials_entity = $variables['elements']['#clinical_trials_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

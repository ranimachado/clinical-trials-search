<?php

/**
 * @file
 * Contains ct_location_entity.page.inc..
 *
 * Page callback for CT Location entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for CT Location templates.
 *
 * Default template: ct_location_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_ct_location_entity(array &$variables) {
  // Fetch CtLocationEntity Entity Object.
  $ct_location_entity = $variables['elements']['#ct_location_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
